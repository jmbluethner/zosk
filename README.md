# ZOSK - Zabbix Kiosk

<i>By <a href="https://jmbluethner.de" target="_blank">jmbluethner</a></i>
<hr>
💻 Setup complexity: Easy<br> 
⌚ Setup time: 20 mins.<br>
📙 Requirements: Webserver <i>(Apache recommended)</i> 
<hr>

## About

ZOSK is an Open Source Kiosk for Zabbix which can be customized to show exactly what you need.  
It can be used to show basic monitoring stats on TV's in your company for example.  
Feel free to use it however you want. Also, feel free to create a fork and customize ZOSK however you want to.  
I'd love to hear your feedback!

## Installation

Unfortunately, the installation is quite complex.  
Gotcha. It's really easy in fact.

Grab all the files from <code>/src</code> and move them to your webservers root directory.  
ZOSK <b>might</b> work in a subdirectory, but that's neither the supported nor the intended way of doing it. So just put it into your webservers root path.

You can now reach ZOSK at your webservers address.  
Let's assume your webserver runs locally on <code>127.0.0.1</code> from now on for this example.

## Configuration

Configuring ZOSK is, you've guessed it, just as easy. It might just take some time to set up.  
Let's start by creating a dedicated Zabbix user for the Kiosk.

### Create Zabbix user and role

> You need a Zabbix user with the "super admin" role, so you can create and manage users, groups and roles. Ask your admin if you are not privileged enough.  

Head over to <code>Administration > User roles</code> and click on <code>Create user role</code>. Let's name it <code>kiosk</code> for the sake of this example.  
The <code>Access to UI elements</code> section doesn't have to be touched. Make sure that there is nothing checked in the <code>Configuration</code> and <code>Administration</code> section though, because that will make your kiosk user way to powerful which might end up in trouble regarding your network security.    
Now scroll down to the <code>Access to actions</code> section below <code>Access to API</code> and get rid of all checkboxes, including <code>Default access to new actions</code>. This is also a precaution to not give the kiosk user more privileges than it really needs.  
Once you are done, click the <code>Add</code> Button and safe the new Role.

Now go to <code>Administration > Users</code> and create a new user.  
We'll give it the username <code>kiosk</code>, feel free to do so too. In the <code>groups</code> section, add the new user to the <code>No access to the frontend</code> group. Set the password for the new user, and go to the <code>Permissions</code> tab at the top of the screen. Now add the kiosk role, which we have just created in the previous step.  
Click <code>Add</code> to create the new user.

### Configure ZOSK

When opening the ZOSK webpage for the first time, without having configured it yet, you'll be asked to create a configuration.  
So - Let's do that now.  

In order to configure ZOSK, we have to edit the config file which can be found under <code>/src/config/config.js</code>  
Here's how to do it  

```js
export const Config = {
    // That's where your Zabbix host goes.
    // Example: https://zabbix.local
    // Works with and witout SSL, though SSL is recommended of course.
    zabbixHost: '',
    // Your kiosk users username
    zabbixKioskUser: '',
    // Your kiosk users password
    zabbixKioskPassword: '',
    // This defines what you want to show on the kiosk.
    kioskDataElements: [
        
    ]
}
```

## Honorable mentions

<a href="https://zabbix.com" target="_blank">Zabbix</a>  
<a href="https://vuejs.org" target="_blank">VueJS</a>  
<a href="https://www.chartjs.org/" target="_blank">Chart.js</a>

<br>
<hr>
<i>Developed with ❤️ and ☕ in Germany</i>