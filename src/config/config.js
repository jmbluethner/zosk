/**
 * @author jmbluethner
 * @description This file/variable is being used to configure ZOSK. For an in depth guide check out the /readme.md file or go to https://gitlab.com/jmbluethner/zosk/-/blob/main/README.md
 * @type {{zabbixHost: string, zabbixKioskUser: string, kioskDataElements: *[], zabbixKioskPassword: string}}
 * @var {Object} config Main config variable for ZOSK
 */

export const Config = {
    zabbixHost: '',
    zabbixKioskUser: '',
    zabbixKioskPassword: '',
    kioskDataElements: [

    ]
}