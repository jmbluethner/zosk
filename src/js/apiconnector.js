import {Config} from "../config/config.js";
import * as Cookiehandler from "./cookiehandler.js";

/**
 * @author jmbluethner <mail@jmbluethner.de>
 * @description This function makes the login request to zabbix in order to get the API token
 * @param {function} callback
 */
function makeLoginCall(callback) {
    if(Cookiehandler.getCookieByName('auth_token')) {
        callback(Cookiehandler.getCookieByName('auth_token'));
    }
    let body = {
        "method": "user.login",
        "params": {
            "user": Config.zabbixKioskUser,
            "password": Config.zabbixKioskPassword
        },
        "id": 1,
        "auth": null
    }
    callApi('POST',body,function (response) {
        if(response.result) {
            Cookiehandler.setCookie('auth_token',response.result,999);
            callback(response.result);
        } else {
            return false;
        }
    });
}

/**
 * @author jmbluethner <mail@jmbluethner.de>
 * @description Global function to make all the required API requests
 * @param {String} method GET,POST,PUT,DELETE
 * @param {Object} body Request body as JSON
 * @param {function} callback Callback function. Contains the response and the status code
 */
export function callApi(method,body,callback) {
    let xhr = new XMLHttpRequest();
    let url = Config.zabbixHost + '/api_jsonrpc.php';
    xhr.onreadystatechange = function() {
        let status = {status: this.status}
        if(xhr.readyState === 4) {
            callback(Object.assign({}, JSON.parse(this.response), status));
        }
    };
    xhr.open(method, url, true);
    xhr.setRequestHeader("Content-Type", "application/json-rpc");
    body.jsonrpc = "2.0";
    if(Cookiehandler.getCookieByName('auth_token')) {
        body.auth = Cookiehandler.getCookieByName('auth_token');
        xhr.send(JSON.stringify(body));
    } else {
        makeLoginCall(function (response) {
            body.auth = response;
            xhr.send(JSON.stringify(body));
        });
    }
}