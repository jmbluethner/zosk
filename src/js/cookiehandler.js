/**
 * @author jmbluethner <mail@jmbluethner.de>
 * @description Get a cookie by its name
 * @param name
 * @returns {string}
 */
export function getCookieByName(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}

/**
 * @author jmbluethner <mail@jmbluethner.de>
 * @description Use this function to set new cookies
 * @param {String} name Cookie name
 * @param {String} value Cookie value
 * @param {Number} days Cookie valid time
 */
export function setCookie(name,value,days) {
    let expires = "";
    if (days) {
        let date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}