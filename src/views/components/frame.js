export const Frame = `
    <div id="content-container">
        <div id="topbar">
            <div class="left">
                <img src="/media/logo.svg" alt="ZOSK Logo" />
            </div>
            <div class="right">
            
            </div>
        </div>
        <div id="router">
            <router-view></router-view>
        </div>
        <div id="footer">
            <span><a href="https://gitlab.com/jmbluethner/zosk" target="_blank">ZOSK by jmbluethner</a></span>
        </div>
    </div>
`