import * as importer from '../js/importer.js';

/**
 * @author jmbluethner <mail@jmbluethner.de>
 * @description Loads Vue and Vue Router
 * @param {function} callback
 */
export function prepareVue(callback) {
    importer.appendJsFile('../lib/vue/vue.js','text/javascript', function () {
        importer.appendJsFile('../lib/vue-router/vue-router.js','text/javascript', function () {
            callback();
        });
    });
}