/**
 * @author jmbluethner <mail@jmbluethner.de>
 */

// Import js
import {prepareVue} from "../main.js";
import {appendCssFile} from "../../js/importer.js";

// Import components
import {Frame} from "../components/frame.js";

// Import router pages
import {Dash} from "../routerpages/dash.js";

// Main Component
prepareVue(function () {

    appendCssFile('../../css/main.css',function () {});


    let bus = {};
    bus.eventBus = new Vue();

    Vue.use(VueRouter)

    // Create router
    const router = new VueRouter({
        routes: [
            {
                path: '/',
                component: Dash,
                name: "Dash"
            }
        ]
    });

    // Create app
    let app = new Vue({
        el: '#app',
        components: {
            frame: Frame
        },
        data: function () {
            return {
                pageData: {}
            }
        },
        mounted: function() {

        },
        router,
        template: Frame
    });
})